import XCTest

import lbflashcardsTests

var tests = [XCTestCaseEntry]()
tests += lbflashcardsTests.allTests()
XCTMain(tests)
