import Kitura
import HeliumLogger
import Foundation
import KituraStencil

HeliumLogger.use()

let router = Router()
router.all(middleware: BodyParser())
router.all("/",middleware: StaticFileServer(path:"./Public"))
router.add(templateEngine: StencilTemplateEngine())
 
// On crée des questions
struct LBQuestion {
    var question: String
    var reponse: String
}

// Créer des matières
struct LBMatiere{
    var libelle: String
    var questions: [LBQuestion]

// Créer d'un taleau vide
init(csvPath: URL){
var result: [LBQuestion] = []

// Différencier le nil et le non-nil
if let file = try? String(contentsOf: csvPath){

// Séparer le file CSV en plusieurs lignes
let csv =  file.components(separatedBy: "\n")

// Séprarer chaque ligne en deux colonnes
for line in csv {
 let columns = line.components(separatedBy: ";")
print(columns) // Afficher les colonnes

if columns.count != 2  {
    continue
}

// Créer une question
 let quest = LBQuestion(question: columns[0], reponse: columns[1])
print(quest) // Afficher la question

// Ajouter la question au tableau crée ci-dessus
    result.append(quest)
}

} else {
    return nil
}

// Création d'un tableau de matières
    var tabMatieres : [LBMatiere] = [];

// Création de la liste des url pour les matières
var listUrl : [String] = []

// Récupérer le nom du fichier de la matière et ajouter au tableau
for url in urls {
    let urlName = url.absoluteString

// Ajouter la matière au tableau d'url
    listUrl.append(urlName)
}

// Créer une matière pour tous les URLS
for x in listUrl{
let mat : LBMatiere? = LBMatiere(csvUrl: URL(string:x)!)

if let mat = mat {
    tabMatieres.append(mat)
}
    
// Renvoyer html page exercice
router.get("/matiere/\(mat.libelle)") { request, response, next in
        try response.render("matiere.stencil", context: ["mat" : mat])
        next()
        }
    }

// Renvoyer html page d'accueil
router.get
    
Kitura.addHTTPServer(onPort: 8080, with: router)
Kitura.run()
